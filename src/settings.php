<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header
        'upload_directory' => __DIR__ . '/../uploads', // upload directory


        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        // "db" => [
        //     'driver' => 'mysql',
        //     'host' => 'difolestari.com',
        //     'database' => 'monitoring_db',
        //     'username' => 'difolest_sigma',
        //     'password' => 'sigma201809',
        //     'collation' => 'utf8_general_ci',
        //     'charset' => 'utf8',
        //     'prefix' => ''
        // ],

         "db" => [
            'driver' => 'mysql',
            'host' => 'localhost',
            'database' => 'monitoring_db',
            'username' => 'root',
            'password' => '',
            'collation' => 'utf8_general_ci',
            'charset' => 'utf8',
            'prefix' => ''
        ],

        //  "db" => [
        //     'driver' => 'mysql',
        //     'host' => 'localhost',
        //     'database' => 'u569530886_monitoring_db',
        //     'username' => 'u569530886_monitoring_use',
        //     'password' => '334455',
        //     'collation' => 'utf8_general_ci',
        //     'charset' => 'utf8',
        //     'prefix' => ''
        // ],


    ],
];
