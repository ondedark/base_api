<?php
namespace App\Middleware;

use App\Models\Sign;
use App\Models\Token;
use Firebase\JWT\JWT;

class Jwtauth
{
     /**
     * Example middleware invokable class
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */

    // Use container to set up our newly created instance of __testReq
    function __construct($container) {
        $this->db= $container->get('db');
    }
    public function __invoke($request, $response, $next)
    {
        //$response->getBody()->write('BEFORE');
        //echo "JWTAuth";
        //$response = $next($request, $response);
        //$response->getBody()->write('AFTER');
        $token = "";
        $sign = "";
         $headers = $request->getHeaders();
         foreach ($headers as $name => $values) {
             //echo $name . ": " . implode(", ", $values);
             if($name == "HTTP_AUTHORIZATION"){
                 $token = $values;
                 //var_dump($token);
             }
         }
         $req_token = trim(substr($token[0], 7));
         //echo $req_token;

         $parsed   = $request->getParsedBody();
            $username = $parsed["username"];
            $useragent = $parsed["useragent"];
            $platform = "Web";
            if (strpos($useragent, "Android") == true  || $useragent == "Android") { 
                $platform = "Android"; 
            } 
            //echo "username:".$username;
        // $response = '{"username"}'$this->getSign($username, $req_token);
        $sign = $this->getSign($username, $req_token);

        $datatoken = $this->validateToken($req_token,$sign,$username,$platform);
        //echo "Token isValid:".$datatoken;
        // $data = (object) [
        //     'username' => $username,
        //     'token' => $req_token,
        //     'sign' => $sign
        // ];

        // $dataJson = json_encode($obj);
        // $request = $request->withAttribute('data', $dataJson);
        if($datatoken == true){
            return $response = $next($request, $response);
        }
        else {
            
            $array = array();
            $data["status"] = "Token Not Match";
            array_push($array, $data);
            return $response->withJson(["status" => "failed", "data" => $array], 200);
        }
        
        

        //return $response;
    }

    //////////////////////////////////////////////
    public function getSign($username, $jwt_token)
    {
        $result_sign    = Sign::where('id_email', '=', $username)->get();
    // print_r(count($result_sign));
        if(count($result_sign) != 0){
            return $result_sign[0]['sign'];
        }else{
            return "failed";
        }
        //var_dump($result_sign);
        

        // $this->exec( $result_sign['sign']);
    }

    public function validateToken($jwt, $sign, $username, $platform)
    {   
        $decoded =  array('sub' => '' , );
        // $stmt_token = $this->dataToken($username,$platform);
        // if ($stmt_token->rowCount() > 0) {
         $result_token   = Token::where('id_email', '=', $username)->where('useragent', '=', $platform)->get();
         if(count($result_token) != 0){ 
            $database_token = $result_token[0]["token"];
            //print_r($result_token);
            // var_dump($result_sign['sign']);
            // $this->exec($result_sign['sign']);
            if($database_token == $jwt){
                 try {
                    $decoded = JWT::decode($jwt, $sign, array('HS256'));
                    $decoded = json_decode(json_encode($decoded),true);
                } catch (\Firebase\JWT\ExpiredException $e) {
                    //print "Error!: " . $e->getMessage() . "";
                    die();
                    return "expired token";
                }
            }
            
            

        }
       
        // var_dump($decoded);
        //echo $iat."/".$jwt;
        if($decoded["sub"] == $username){
            return true;
        }else{
            
           return false; 
        }
        

        

    }

    public function dataToken($username,$platform)
    {
        $token           = new Token($this->db);
        $token->id_email = $username;
        $token->useragent = $platform;
        $stmt_token      = $token->checkToken();
        return $stmt_token;

    }
    public function dataSign($username)
    {
        $sign           = new Sign($this->db);
        $sign->username = $username;
        $stmt_sign      = $sign->getSign();
        return $stmt_sign;
    }

    // public function exec($sign)
    // {
    //     $container = $this->app->getContainer();

    //     $this->container['logger'] = function ($c) {
    //         $logger       = new \Monolog\Logger('my_logger');
    //         $file_handler = new \Monolog\Handler\StreamHandler("../logs/app.log");
    //         $logger->pushHandler($file_handler);
    //         return $logger;
    //     };

    //     $container["jwt"] = function ($container) {
    //         return new StdClass;
    //     };
    //     //$sign = $this->sign_data;//"4646A82115A163813F49E06A184056E3ECF28E10D8E6E2C2CEFDFF47C691B5E4";

    //     // $this->app->add($getSign);
    //     $this->app->add(new \Slim\Middleware\JwtAuthentication([
    //         "path"     => "/",
    //         "logger"   => $container['logger'],
    //         //"secret" => "4646A82115A163813F49E06A184056E3ECF28E10D8E6E2C2CEFDFF47C691B5E4",
    //         "secret"   => $sign,
    //         "rules"    => [
    //             new \Slim\Middleware\JwtAuthentication\RequestPathRule([
    //                 "path"        => "/",
    //                 "passthrough" => ["/token", "/not-secure", "/login"],
    //             ]),
    //             new \Slim\Middleware\JwtAuthentication\RequestMethodRule([
    //                 "passthrough" => ["OPTIONS"],
    //             ]),
    //         ],
    //         "callback" => function ($request, $response, $arguments) use ($container) {
    //             $container["jwt"] = $arguments["decoded"];
    //             //var_dump($container['jwt']);
    //             //var_dump($container["jwt"]);
    //         },
    //         "error"    => function ($request, $response, $arguments) {
    //             $data["status"]  = "error";
    //             $data["message"] = $arguments["message"];
    //             return $response
    //                 ->withHeader("Content-Type", "application/json")
    //                 ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    //         },
    //     ]));

    //     // $this->app->add(new \Slim\Middleware\HttpBasicAuthentication([
    //     //     "path" => "/api/token",
    //     //     "users" => [
    //     //         "user" => "password"
    //     //     ]
    //     // ]));

    //     $this->app->add(new \Tuupola\Middleware\Cors([
    //         "logger"         => $container["logger"],
    //         "origin"         => ["*"],
    //         "methods"        => ["GET", "POST", "PUT", "PATCH", "DELETE"],
    //         "headers.allow"  => ["Authorization", "If-Match", "If-Unmodified-Since"],
    //         "headers.expose" => ["Authorization", "Etag"],
    //         "credentials"    => true,
    //         "cache"          => 60,
    //         "error"          => function ($request, $response, $arguments) {
    //             return new UnauthorizedResponse($arguments["message"], 401);
    //         },
    //     ]));

    // }

}

