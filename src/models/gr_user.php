<?php  
namespace App\Models;
 
class Gr_user extends \Illuminate\Database\Eloquent\Model {  
  protected $table = "gr_user";
       public $timestamps = false;
  public $incrementing = false;
  // protected  $primaryKey = 'id_st_huni';
   // protected $fillable = ['body'];
   protected $fillable = ["id","ket"];
//   public function mp_tb_rfid_produks()
//     {
//         return $this->hasMany('App\Models\Mp_tb_rfid_produk');
//     }

    public function akses_menu()
    {
        return $this->hasMany('App\Models\Akses_menu');
    }
     public function user()
    {
        return $this->hasMany('App\Models\User');
    }
}