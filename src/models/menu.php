<?php  
namespace App\Models;
 
class Menu extends \Illuminate\Database\Eloquent\Model {  
  protected $table = "menu";
       public $timestamps = false;
  public $incrementing = false;
  // protected  $primaryKey = 'id_st_huni';
   // protected $fillable = ['body'];
   protected $fillable = ["id","ket_menu","url_menu","icon"];
//   public function mp_tb_rfid_produks()
//     {
//         return $this->hasMany('App\Models\Mp_tb_rfid_produk');
//     }

    public function akses_menu()
    {
        return $this->hasMany('App\Models\Akses_menu');
    }
}