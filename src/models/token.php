<?php  
namespace App\Models;
 
class Token extends \Illuminate\Database\Eloquent\Model {  
  protected $table = "u_tokn";
       public $timestamps = false;
  public $incrementing = false;
  protected  $primaryKey = ['id_email','useragent'];
   // protected $fillable = ['body'];
   protected $fillable = ['id_email','useragent','token','expired','valid','iat'];
//   public function mp_tb_rfid_produks()
//     {
//         return $this->hasMany('App\Models\Mp_tb_rfid_produk');
//     }
}