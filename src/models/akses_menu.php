<?php  
namespace App\Models;
 
class Akses_menu extends \Illuminate\Database\Eloquent\Model {  
  protected $table = "akses_menu";
       public $timestamps = false;
  public $incrementing = false;
  // protected  $primaryKey = 'id_st_huni';
   // protected $fillable = ['body'];
   protected $fillable = ["id","gr_user_id","menu_id"];
//   public function mp_tb_rfid_produks()
//     {
//         return $this->hasMany('App\Models\Mp_tb_rfid_produk');
//     }

    // public function data_bangunan()
    // {
    //     return $this->hasMany('App\Models\Data_bangunan');
    // }
    public function gr_user()
    {
        return $this->belongsTo('App\Models\Gr_user');
    }
     public function menu()
    {
        return $this->belongsTo('App\Models\Menu');
    }
}