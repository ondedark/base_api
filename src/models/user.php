<?php  
namespace App\Models;
 
class User extends \Illuminate\Database\Eloquent\Model {  
  protected $table = "u_user";
       public $timestamps = false;
  public $incrementing = false;
  protected  $primaryKey = ['id_prshn','id_email'];
   // protected $fillable = ['body'];
   protected $fillable = ['id_prshn',
   'id_email',
   	'tx_password',
   	'tx_pswd',
   	'tx_user',
   	'id_jrbyr',
   	'gr_mitra',
    'gr_user_id',
   	'fl_foto',
   	'gr_user',
   	'tx_blok',
   	'tg_dibuat',
   	'tx_dibuat',
   	'tg_diubah',
	'tx_diubah',
	'android_bs',
	'android_rfid'];
//   public function mp_tb_rfid_produks()
//     {
//         return $this->hasMany('App\Models\Mp_tb_rfid_produk');
//     }
   public function gr_user()
    {
        return $this->belongsTo('App\Models\Gr_user');
    }
}