<?php  
namespace App\Models;
 
class Sign extends \Illuminate\Database\Eloquent\Model {  
  protected $table = "u_sign";
       public $timestamps = false;
  public $incrementing = false;
  protected  $primaryKey = 'id_email';
   // protected $fillable = ['body'];
   protected $fillable = ['id_email','sign','iat'];
//   public function mp_tb_rfid_produks()
//     {
//         return $this->hasMany('App\Models\Mp_tb_rfid_produk');
//     }
}