<?php
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;
use App\Middleware\Jwtauth;
use App\Models\Sign;
use App\Models\Token;
use App\Models\User;
use App\Models\Gr_user;
use App\Models\Akses_menu;
use App\Models\Menu;
use Tuupola\Base62;
use Firebase\JWT\JWT;

// Routes

$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->post("/token", function ($request, $response, $args) use ($container) {
    /* Here generate and return JWT to the client. */
    //$valid_scopes = ["read", "write", "delete"]
    $requested_scopes = $request->getParsedBody() ?: [];
    $username         = $requested_scopes["username"];
    $useragent         = $requested_scopes["useragent"];
    $platform = "Web";
    if (strpos($useragent, "Android") == true || $useragent == "Android") { 
        $platform = "Android"; 
    } 

    //Check if email exist in table sign
    $result_sign    = Sign::where('id_email', '=', $username)->get();
    // print_r(count($result_sign));
    if(count($result_sign) != 0){
        // print_r($result_sign);
        $now     = new DateTime();
        $future  = new DateTime("+1 year");
        $server  = $request->getServerParams();
        $jti     = (new Base62)->encode(random_bytes(16));
        $payload = [
            "iat" => $now->getTimeStamp(),
            "exp" => $future->getTimeStamp(),
            "jti" => $jti,
            "sub" => $requested_scopes["username"],
        ];
        $secret      = $result_sign[0]['sign'];
        $jwt_token   = JWT::encode($payload, $secret, "HS256");
        
        //save Token
        //Check if email exist in table token
        // $token_email = $username;
        // $token_ua = $platform;
        // $token_jwt    = $jwt_token;
        $token_expired  = $future->format('Y-m-d H:i:s');
        $token_valid    = 1;
        $token_iat      = $now->getTimeStamp();
        $result_token   = Token::where('id_email', '=', $username)->where('useragent', '=', $platform)->get();
         if(count($result_token) != 0){ 
             $result = Token::where('id_email','=', $username)->where('useragent', '=', $platform)->update([
                'token' => $jwt_token,
                'expired' => $token_expired,
                'valid' => $token_valid,
                'iat' => $token_iat,
                // 'useragent' => $platform
            ]);
            $final_result["pta"] = $jwt_token;
            // $result["sign"] = $secret;
            $final_result["iat"] = $token_iat;
            // $stmt_token = $token->updateToken();
            // $data["db"] = $stmt_token;
        } else {
             $result = Token::create([
                'id_email' => $username,
                'token' => $jwt_token,
                'expired' => $token_expired,
                'valid' => $token_valid,
                'iat' => $token_iat,
                'useragent' => $platform
             ]);
             $final_result["pta"] = $jwt_token;
            // $result["sign"] = $secret;
            $final_result["iat"] = $token_iat;

        }
        
        
    }
    else{
         $final_result["pta"] = "FAILED";
            // $result["sign"] = $secret;
        $final_result["iat"] = $token_iat;
        $final_result["result"] = $result_sign;
    }

    
    // return $response->withStatus(201)
    //     ->withHeader("Content-Type", "application/json")
    //     ->write(json_encode(["status" => "success", "data" => $result], JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
     return $response->withJson(["status" => "success", "data" => $final_result], 200);
});


$app->post("/login/", function ($request, $response, $args) {

    // $query = "SELECT * FROM u_user WHERE id_email =:id_email AND tx_password =:tx_password AND tx_blok =:tx_blok";
    // $stmt = $this->db->prepare($query);

    // $data = [
    //     ":id_email" => $parsed["username"],
    //     ":tx_password" => $parsed["password"],
    //     ":tx_blok" => ""
    // ];

    // if($stmt->execute($data)){
    //    $result = $stmt->fetch();
    //    return $response->withJson(["status" => "success", "data" => $result], 200);
    // }

    // return $response->withJson(["status" => "failed", "data" => "0"], 200);
    $parsed   = $request->getParsedBody();
    $username = $parsed["username"];
    $password = $parsed["password"];
    $useragent = $parsed["useragent"];
    $platform = "Web";
    if (strpos($useragent, "Android") == true || $useragent == "Android") { 
        $platform = "Android"; 
    } 
    $platform = $useragent;
    // echo "Username:".$username;
    // $result_token   = Token::where('id_email', '=', $username)->
    // where('useragent', '=', $platform)->
    // get();
    // if(count($result_token) == 0){ 
    //     $data["id_email"] = "Token Not found";
    //     return $response->withJson(["status" => "failed", "data" => $data], 200);
    // } else {
         
        
    // }
    $result_user   = User::with('gr_user')->where([['id_email', '=', $username],['tx_password', '=', $password]])->get();
         if(count($result_user) == 0){ 
            $data["data"] = "Wrong Username Or Password";
            return $response->withJson(["status" => "failed", "data" => $data], 200);
        }else{
            return $response->withJson(["status" => "success", "data" => $result_user ], 200);;
        }

});

$app->group('/api', function () use ($app) {
    $container_app = $this->getContainer();

     $app->post("/check_valid/", function ($request, $response, $args ) {
        $parsed            = $request->getParsedBody();
        $username        = $parsed["username"];
        $useragent        = $parsed["useragent"];
        $platform = "Web";
        if (strpos($useragent, "Android") == true || $useragent == "Android") { 
            $platform = "Android"; 
        }
        if (!empty($parsed["username"])) {
             $array = array();
            $data["status"] = "Token is Good";
            array_push($array, $data); 
            return $response->withJson(["status" => "success", "data" => $array], 200);
        }else{
             $array = array();
            $data["status"] = "Token not Valid";
            array_push($array, $data); 
            return $response->withJson(["status" => "failed", "data" => $array], 200);
        }

        // echo "Usernamefrom api:".$username;
        // $result_token = Token::where('id_email', '=', $username)->where('useragent', '=', $useragent)->delete();
        // $result_token->destroy();
        
       

        // return $response->withJson(["status" => "success", "data" => $final_result], 200);
     })->add( new Jwtauth($container_app)  );

     $app->post("/logout/", function ($request, $response, $args ) {
        $parsed            = $request->getParsedBody();
        $username        = $parsed["username"];
        $useragent        = $parsed["useragent"];
        $platform = "Web";
        if (strpos($useragent, "Android") == true || $useragent == "Android") { 
            $platform = "Android"; 
        } 
        // echo "Usernamefrom api:".$username;
        $result_token = Token::where('id_email', '=', $username)->where('useragent', '=', $useragent)->delete();
        // $result_token->destroy();
        
         if($result_token != 0){
             $final_result = ["status" => "success", "data" => $result_token];
         }else{
             $final_result = ["status" => "failed", "data" => $result_token];
         }
        return $response->withJson($final_result, 200);

        // return $response->withJson(["status" => "success", "data" => $final_result], 200);
     })->add( new Jwtauth($container_app)  );

      $app->post("/get_menu/", function ($request, $response, $args ) {
        $parsed            = $request->getParsedBody();
        $username        = $parsed["username"];
        $gr_user_id        = $parsed["gr_user_id"];
        $useragent        = $parsed["useragent"];
        $platform = "Web";
        if (strpos($useragent, "Android") == true || $useragent == "Android") { 
            $platform = "Android"; 
        } 
        // echo "Usernamefrom api:".$username;
        $result_menu = Akses_menu::with('menu')->where('gr_user_id', '=', $gr_user_id)->get();
        // $result_token->destroy();
        
         if(count($result_menu) != 0){
             $final_result = ["status" => "success", "data" => $result_menu];
         }else{
             $final_result = ["status" => "failed", "data" => $result_menu];
         }
        return $response->withJson($final_result, 200);

        // return $response->withJson(["status" => "success", "data" => $final_result], 200);
     })->add( new Jwtauth($container_app)  );

     $app->post("/get_gr_user/", function ($request, $response, $args ) {
        $parsed            = $request->getParsedBody();
        $username        = $parsed["username"];
        $gr_user_id        = $parsed["gr_user_id"];
        $useragent        = $parsed["useragent"];
        $platform = "Web";
        if (strpos($useragent, "Android") == true || $useragent == "Android") { 
            $platform = "Android"; 
        } 
        // echo "Usernamefrom api:".$username;
        $result_menu = Gr_user::where('id', '=', $gr_user_id)->get();
        // $result_token->destroy();
        
         if(count($result_menu) != 0){
             $final_result = ["status" => "success", "data" => $result_menu];
         }else{
             $final_result = ["status" => "failed", "data" => $result_menu];
         }
        return $response->withJson($final_result, 200);

        // return $response->withJson(["status" => "success", "data" => $final_result], 200);
     })->add( new Jwtauth($container_app)  );

     $app->post("/get_gr_user_all/", function ($request, $response, $args ) {
        $parsed            = $request->getParsedBody();
        $username        = $parsed["username"];
        // $gr_user_id        = $parsed["gr_user_id"];
        $useragent        = $parsed["useragent"];
        $platform = "Web";
        if (strpos($useragent, "Android") == true || $useragent == "Android") { 
            $platform = "Android"; 
        } 
        // echo "Usernamefrom api:".$username;
        $result_menu = Gr_user::get();
        // $result_token->destroy();
        
         if(count($result_menu) != 0){
             $final_result = ["status" => "success", "data" => $result_menu];
         }else{
             $final_result = ["status" => "failed", "data" => $result_menu];
         }
        return $response->withJson($final_result, 200);

        // return $response->withJson(["status" => "success", "data" => $final_result], 200);
     })->add( new Jwtauth($container_app)  );


     $app->post('/get_user_all/', function ($request, $response, $args) {
        // var_dump($data);
        //echo $email;
        $result = User::select('id_email','tx_user')->get();
        
         if(count($result) != 0){
             $final_result = ["status" => "success", "data" => $result];
         }else{
             $final_result = ["status" => "failed", "data" => $result];
         }
        return $response->withJson($final_result, 200);
    })->add( new Jwtauth($container_app)  );
    
        $app->post('/get_user/', function ($request, $response, $args) {
        // var_dump($data);
        //echo $email;
        $parsed            = $request->getParsedBody();
        $username        = $parsed["username"];
        $useragent        = $parsed["useragent"];
        $result = User::select('id_email','tx_user')->
        where('id_email','=',$username)->
        get();
        
         if(count($result) != 0){
             $final_result = ["status" => "success", "data" => $result];
         }else{
             $final_result = ["status" => "failed", "data" => $result];
         }
        return $response->withJson($final_result, 200);
    })->add( new Jwtauth($container_app)  );
    
      $app->post('/do_edit_pass_user/', function ($request, $response, $args) {
        // var_dump($data);
        //echo $email;
        $parsed            = $request->getParsedBody();
        $username        = $parsed["username"];
        $password        = $parsed["password"];
        $newpassword        = $parsed["newpassword"];
        $result = User::where('id_email', '=', $username)->where('tx_password', '=', $password)->get();
        //  print_r($result);
         if(count($result) != 0){
             
            $result_update = User::where("id_email","=",$username)->where("tx_password","=",$password)->update(["tx_password" => $newpassword]);
            
             if($result_update != 0){
                 $final_result = ["status" => "success", "data" => $result_update]; 
             }
             else{
               $final_result = ["status" => "failed", "data" => $result];  
             }
            
         }else{
             $final_result = ["status" => "failed", "data" => $result]; 
         }
        return $response->withJson($final_result, 200);
    })->add( new Jwtauth($container_app)  );
    
      $app->post('/do_add_user/', function ($request, $response, $args) {
        // var_dump($data);
        //echo $email;
        $parsed            = $request->getParsedBody();
        $username        = $parsed["username"];
        $password        = $parsed["password"];
        $tx_user        = $parsed["tx_user"];
        $gr_user_id        = $parsed["gr_user_id"];
        $sign = generateRandomString();
        $result = User::create(["id_email"=>$username,"tx_password"=>$password,"tx_user"=>$tx_user,"gr_user_id"=>$gr_user_id]);
        
        //  print_r($result);
         if($result){
             
             $result_sign = Sign::create(["id_email"=>$username,"sign"=>$sign]);
             if($result_sign){
                 $final_result = ["status" => "success", "data" => $result]; 
             }else{
                 $final_result = ["status" => "failed", "data" => $result]; 
             }
            
         }else{
             $final_result = ["status" => "failed", "data" => $result]; 
         }
        return $response->withJson($final_result, 200);
    });
     $app->post('/do_delete_user/', function ($request, $response, $args) {
        $data= $request->getParsedBody();
        $id_email = htmlspecialchars(strip_tags($data["username"]));
        // var_dump($data);
        //echo $email;
        $result = User::where('id_email', $id_email)->delete();
        if($result != 0){
            $result_sign = Sign::where('id_email', $id_email)->delete();
            $result_token = Token::where('id_email', $id_email)->delete();
             $final_result = ["status" => "success", "data" => $result];
         }else{
             $final_result = ["status" => "failed", "data" => $result];
         }
        return $response->withJson($final_result, 200);
            
    });
});




function generateRandomString($length = 25) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}




